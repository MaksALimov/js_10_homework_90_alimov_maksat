const express = require('express');
const {nanoid} = require('nanoid');
const app = express();
const cors = require('cors');

require('express-ws')(app);

const port = 8000;

app.use(cors());

const defaultCanvas = {
    type: 'FIRST_CONNECT',
    mouseDown: false,
    pixelsArray: []
};

const activeConnections = {};

app.ws('/canvas', (ws, req) => {
    const id = nanoid();

    activeConnections[id] = ws;

    ws.send(JSON.stringify(defaultCanvas));

    ws.on('message', msg => {
        const decoded = JSON.parse(msg);

        defaultCanvas.pixelsArray = decoded.pixelsArray;

        switch (decoded.type) {
            case 'DREW_DOTS':
                Object.keys(activeConnections).forEach(userId => {
                    const userConnection = activeConnections[userId];

                    userConnection.send(JSON.stringify({
                        type: 'DRAWN_DOTS',
                        pixelsArray: decoded.pixelsArray,
                        mouseDown: decoded.mouseDown,
                    }));
                });
                break;
            default:
                console.log('Unknown type');
        }
    });

    ws.on('close', () => {
        delete activeConnections[id];
    });
});

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});

