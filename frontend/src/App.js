import React, {useEffect, useRef, useState} from "react";

const App = () => {
    const ws = useRef(null);
    const canvas = useRef(null);

    const [state, setState] = useState({
        mouseDown: false,
        pixelsArray: []
    });

    useEffect(() => {
        ws.current = new WebSocket('ws://localhost:8000/canvas');

        ws.current.onmessage = event => {
            const decoded = JSON.parse(event.data);

            const context = canvas.current.getContext('2d');
            const imageData = context.createImageData(1, 1);
            const d = imageData.data;

            switch (decoded.type) {
                case 'DRAWN_DOTS':
                    setState(prevState => ({
                        ...prevState,
                        mouseDown: decoded.mouseDown,
                        pixelsArray: decoded.pixelsArray
                    }));

                    d[0] = 0;
                    d[1] = 0;
                    d[2] = 0;
                    d[3] = 255;

                    for (const key of decoded.pixelsArray) {
                        context.putImageData(imageData, key.x, key.y);
                    }

                    break;

                case 'FIRST_CONNECT':
                    setState(prevState => ({
                        ...prevState,
                        mouseDown: decoded.mouseDown,
                        pixelsArray: decoded.pixelsArray
                    }));
                    d[0] = 0;
                    d[1] = 0;
                    d[2] = 0;
                    d[3] = 255;

                    for (const key of decoded.pixelsArray) {
                        context.putImageData(imageData, key.x, key.y);
                    }
                    break;
                default:
                    console.log('Unknown type');
            }
        }

    }, []);

    const canvasMouseMoveHandler = event => {
        if (state.mouseDown) {
            const clientX = event.clientX;
            const clientY = event.clientY;

            setState(prevState => {
                return {
                    ...prevState,
                    pixelsArray: [...prevState.pixelsArray, {
                        x: clientX,
                        y: clientY
                    }]
                };
            });

            const context = canvas.current.getContext('2d');
            const imageData = context.createImageData(1, 1);
            const d = imageData.data;

            d[0] = 0;
            d[1] = 0;
            d[2] = 0;
            d[3] = 255;
            context.putImageData(imageData, event.clientX, event.clientY);
        }
    };

    const mouseDownHandler = () => {
        setState({...state, mouseDown: true});
    };

    const mouseUpHandler = () => {
        ws.current.send(JSON.stringify({
            type: 'DREW_DOTS',
            pixelsArray: state.pixelsArray,
            mouseDown: false,
        }));
        setState({...state, mouseDown: false, pixelsArray: []});

    };
    return (
        <div>
            <canvas
                ref={canvas}
                style={{border: '1px solid black'}}
                width={800}
                height={600}
                onMouseDown={mouseDownHandler}
                onMouseUp={mouseUpHandler}
                onMouseMove={canvasMouseMoveHandler}
            />
        </div>
    );
};

export default App;
